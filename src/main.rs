mod generic_shape;
use generic_shape::*;

fn main() {
    let circle1=Circle{
        r:f64::from(10)
    };
    println!("{:#?} area {}\n",circle1,area(&circle1));

    let circle2=Circle{
        r:10
    };
    println!("{:#?} area {}\n",circle2,area(&circle2));


    let triangle1=Triangle{
        a:f64::from(3),
        b:f64::from(4.0),
        c:f64::from(5.0),
    };
    println!("{:#?} area {}\n",triangle1,area(&triangle1));

    let triangle2=Triangle{
        a:3,
        b:4.0,
        c:5.0000000000001,
    };
    println!("{:#?} area {}\n",triangle2,area(&triangle2));

    let square1=Square{
        a:f64::from(10),
    };
    println!("{:#?} area {}\n",square1,area(&square1));
}