use std::ops::Mul;
use std::f64::consts::PI;

#[derive(Debug)]
pub struct Circle<T>{
    pub r:T
}

#[derive(Debug)]
pub struct Triangle<T,U,V>{
    pub a:T,
    pub b:U,
    pub c:V,
}

#[derive(Debug)]
pub struct Square<T>{
    pub a:T,
}

pub trait AreaCalculate{
    fn area(&self) -> f64;
}

impl<T: Mul + Copy> AreaCalculate for Circle<T>
    where f64: From<<T as Mul>::Output> {
    fn area(&self) -> f64 {
        PI * f64::from( self.r * self.r)
    }
}

impl<T: Copy> AreaCalculate for Square<T>
    where f64: From<T>  {
    fn area(&self) -> f64 {
        let a:f64 = f64::from(self.a);
        a * a
    }
}


impl<T,U,V> AreaCalculate for Triangle<T,U,V>
    where f64: From<T>,f64: From<U>,f64: From<V>,
        T:Copy, U:Copy, V:Copy{
    fn area(&self) -> f64 {
        let Triangle{a,b,c}=self;
        let a:f64=f64::from(*a);
        let b:f64=f64::from(*b);
        let c:f64=f64::from(*c);
        let p=(a+b+c)/2.0;

        (p*(p-a)*(p-b)*(p-c)).sqrt()
    }
}

pub fn area<T: AreaCalculate>(shape: &T) -> f64{
    shape.area()
}
