use std::f64::consts::PI;

#[derive(Debug)]
pub struct Circle{
    pub r:f64
}

#[derive(Debug)]
pub struct Triangle{
    pub a:f64,
    pub b:f64,
    pub c:f64,
}

#[derive(Debug)]
pub struct Square{
    pub a:f64,
}

pub trait AreaCalculate{
    fn area(&self) -> f64;
}

impl AreaCalculate for Circle{
    fn area(&self) -> f64 {
        PI * self.r * self.r
    }
}

impl AreaCalculate for Square{
    fn area(&self) -> f64 {
        self.a*self.a
    }
}

impl AreaCalculate for Triangle{
    fn area(&self) -> f64 {
        let Triangle{a,b,c}=self;
        let p=(a+b+c)/2.0;

        (p*(p-a)*(p-b)*(p-c)).sqrt()
    }
}


pub fn area<T: AreaCalculate>(shape: &T) -> f64{
    shape.area()
}
